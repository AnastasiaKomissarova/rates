package akomissarova.currencyconverter.rates_list.ui

import akomissarova.currencyconverter.R
import akomissarova.currencyconverter.rates_list.data.RateItem
import android.annotation.SuppressLint
import android.content.Context
import android.text.Editable
import android.text.TextWatcher
import android.view.*
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.cottacush.android.currencyedittext.CurrencyEditText
import com.squareup.picasso.Picasso
import jp.wasabeef.picasso.transformations.CropCircleTransformation
import java.math.BigDecimal
import java.text.DecimalFormat
import java.text.DecimalFormatSymbols
import java.util.*

const val AMOUNT_FIELD = "amount"

class RatesListAdapter(
    private val context: Context,
    private val itemFocusListener: (itemId: String) -> Unit,
    private val keyboardListener: (isVisible: Boolean) -> Unit,
    private val amountChangedListener: (chosenItem: RateItem) -> Unit
) : RecyclerView.Adapter<CurrencyListViewHolder>() {

    private val currencyNames = CurrencyName(context)
    private var items = mutableListOf<RateItem>()
    private val decimalFormat = DecimalFormat("#,##0.00").also {
        val symbols = DecimalFormatSymbols(Locale.getDefault()).apply {
            decimalSeparator = '.'
            groupingSeparator = ','
        }
        it.decimalFormatSymbols = symbols
    }

    fun updateList(newValue: List<RateItem>) {
        val diffCallback = RateDiffCallback(items, newValue)
        val diffResult = DiffUtil.calculateDiff(diffCallback)
        items.clear()
        items.addAll(newValue)
        diffResult.dispatchUpdatesTo(this)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CurrencyListViewHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.rates_list_item, parent, false)

        return CurrencyListViewHolder(
            itemView,
            decimalFormat,
            currencyNames,
            keyboardListener
        ) {
            val updatedItem = RateItem(
                id = items[0].id,
                rate = items[0].rate,
                amount = it
            )
            amountChangedListener.invoke(updatedItem)
        }
    }

    fun getChosenItem() = if (items.size > 0) items[0] else null

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: CurrencyListViewHolder, position: Int) {
        holder.setItemInfo(items[position]) {
            moveItemToTop(it)
        }
    }

    override fun onBindViewHolder(
        holder: CurrencyListViewHolder,
        position: Int,
        payloads: MutableList<Any>
    ) {
        if (payloads.isEmpty()) {
            super.onBindViewHolder(holder, position, payloads)
        }
        holder.updateAmount(items[position].amount)
    }

    private fun moveItemToTop(oldPosition: Int) {
        val newOrderItems = mutableListOf<RateItem>()
        newOrderItems.add(items[oldPosition])
        items.forEach {
            if (it.id != items[oldPosition].id) {
                newOrderItems.add(it)
            }
        }

        updateList(newOrderItems)
        itemFocusListener.invoke(newOrderItems[0].id)
    }

    private fun getFirstPosition(): Int = 0

}

class CurrencyListViewHolder(
    itemView: View,
    private val decimalFormat: DecimalFormat,
    private val currencyNames: CurrencyName,
    private val keyboardListener: (isVisible: Boolean) -> Unit,
    private val amountChangedListener: (amount: BigDecimal) -> Unit
) : RecyclerView.ViewHolder(itemView) {

    private var title = itemView.findViewById<TextView>(R.id.currencyTitle)
    private var subtitle = itemView.findViewById<TextView>(R.id.currencySubtitle)
    private var icon = itemView.findViewById<ImageView>(R.id.currencyIcon)
    private var amountView =
        itemView.findViewById<CurrencyEditText>(R.id.currencyAmount).also { it.clearFocus()
            it.addTextChangedListener(createTextWatcher(it))
        }

    @SuppressLint("ClickableViewAccessibility")
    fun setItemInfo(
        dataItem: RateItem,
        itemFocusListener: (position: Int) -> Unit
    ) {
        title.text = dataItem.id
        subtitle.text = currencyNames.get(dataItem.id)
        loadImage()
        itemView.setOnClickListener {
            if (amountView.hasFocus()) {
                keyboardListener.invoke(true)
            } else {
                activateEdit(itemFocusListener)
            }
        }
        amountView.setOnTouchListener { _, event ->
            if (event.action == MotionEvent.ACTION_UP) {
                activateEdit(itemFocusListener)
                return@setOnTouchListener true
            }
            false
        }
        amountView.setOnKeyListener { _, keyCode, _ ->
            if (keyCode == KeyEvent.KEYCODE_ENTER) {
                keyboardListener.invoke(false)
                return@setOnKeyListener true
            }
            false
        }
        amountView.setOnFocusChangeListener { _, hasFocus ->
            if (hasFocus) {
                keyboardListener.invoke(true)
            }
        }

    }

    private fun formatCurrencyText(newAmount: BigDecimal): String {
        return if (newAmount > BigDecimal.ZERO) {
            var formattedString = decimalFormat.format(newAmount)
            if (formattedString.contains(".")) {
                repeat((0..2).count()) {
                    if (formattedString.last() == '0' || formattedString.last() == '.') {
                        formattedString = formattedString.substring(0, formattedString.length - 1)
                    }
                }
            }
            formattedString
        } else {
            "0"
        }
    }

    private fun loadImage() {
        Picasso.get().load(icon.context.getString(R.string.default_image_url))
            .transform(CropCircleTransformation())
            .placeholder(R.drawable.ic_launcher_foreground).into(icon)
    }

    private fun createTextWatcher(editText: CurrencyEditText): TextWatcher {
        return object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                if (editText.hasFocus() && s?.length ?: 0 > 0) {
                    if (editText.getNumericValueBigDecimal() <= MAX_AMOUNT) {
                        amountChangedListener.invoke(editText.getNumericValueBigDecimal())
                    } else {
                        amountView.setText(formatCurrencyText(MAX_AMOUNT))
                        amountChangedListener.invoke(MAX_AMOUNT)
                    }
                } else if (editText.hasFocus()) {
                    amountChangedListener.invoke(BigDecimal.ZERO)
                }
            }

            override fun beforeTextChanged(
                s: CharSequence?,
                start: Int,
                count: Int,
                after: Int
            ) {}

            override fun onTextChanged(
                s: CharSequence?,
                start: Int,
                before: Int,
                count: Int
            ) {}

        }
    }

    private fun activateEdit(itemFocusListener: (position: Int) -> Unit) {
        itemFocusListener.invoke(adapterPosition)
        amountView.requestFocus()
    }

    fun updateAmount(newAmount: BigDecimal) {
        if (!amountView.hasFocus()) {
            amountView.setText(formatCurrencyText(newAmount))
        }
    }

    companion object {
        val MAX_AMOUNT = 100000000.toBigDecimal()
    }

}

