package akomissarova.currencyconverter.rates_list.ui

import akomissarova.currencyconverter.R
import android.content.Context

const val ratesNamesRes = R.array.rates_names

class CurrencyName(private val context: Context) {

    private val currencies = getNames()

    private fun getNames(): Map<String, String> {
        val stringArray: Array<String> =
            context.resources.getStringArray(ratesNamesRes)
        val map = mutableMapOf<String, String>()
        stringArray.forEach {
            val fullName = it.split("|")
            map[fullName[0]] = fullName[1]
        }
        return map
    }

    fun get(id: String) =
        currencies[id] ?: "placeholder"

}
