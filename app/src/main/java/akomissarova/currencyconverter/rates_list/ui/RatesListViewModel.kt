package akomissarova.currencyconverter.rates_list.ui

import akomissarova.currencyconverter.rates_list.data.PollingResult
import akomissarova.currencyconverter.rates_list.data.RateItem
import akomissarova.currencyconverter.rates_list.polling.IRatesGetter
import akomissarova.currencyconverter.rates_list.network.IObservableNetwork
import androidx.lifecycle.*

class RatesListViewModel(private val network: IObservableNetwork,
                         private val ratesGetter: IRatesGetter
) : ViewModel() {

    private val currencyId: MutableLiveData<String> = MutableLiveData()
    private val mainItem: MutableLiveData<RateItem?> = MutableLiveData()

    private val pollingResult: LiveData<PollingResult> = Transformations.switchMap(currencyId.distinctUntilChanged()) {
        LiveDataReactiveStreams.fromPublisher(ratesGetter.startRatesPolling(it))
    }

    private val prices: LiveData<List<RateItem>> = Transformations.switchMap(mainItem.distinctUntilChanged()) {
        if (it == null) {
            LiveDataReactiveStreams.fromPublisher(ratesGetter.loadRates())
        } else {
            LiveDataReactiveStreams.fromPublisher(ratesGetter.recalculateValues(it))
        }
    }

    fun getPollingResult(): LiveData<PollingResult> {
        return pollingResult
    }

    fun setBaseValue(chosenItem: String) {
        currencyId.value = chosenItem
    }

    fun setNewAmount(activeItem: RateItem) {
        mainItem.value = activeItem
    }

    fun loadRates(): LiveData<List<RateItem>> {
        return prices
    }

    fun triggerLocalData() {
        mainItem.value = null
    }

    fun startPolling() {
        network.init()
        setBaseValue("")
    }

    override fun onCleared() {
        network.clear()
    }

}
