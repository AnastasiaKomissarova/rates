package akomissarova.currencyconverter.rates_list.ui

import akomissarova.currencyconverter.rates_list.data.RateItem
import android.os.Bundle
import androidx.recyclerview.widget.DiffUtil
import timber.log.Timber

class RateDiffCallback(
    private val oldRatesList: List<RateItem>,
    private val newRatesList: List<RateItem>
) :
    DiffUtil.Callback() {
    override fun getOldListSize(): Int {
        return oldRatesList.size
    }

    override fun getNewListSize(): Int {
        return newRatesList.size
    }

    override fun areItemsTheSame(
        oldItemPosition: Int,
        newItemPosition: Int
    ): Boolean {
        return oldRatesList[oldItemPosition].id == newRatesList[newItemPosition].id
    }

    override fun areContentsTheSame(
        oldItemPosition: Int,
        newItemPosition: Int
    ): Boolean {
        val oldRate: RateItem = oldRatesList[oldItemPosition]
        val newRate: RateItem = newRatesList[newItemPosition]
        return oldRate.rate == newRate.rate && oldRate.amount == newRate.amount
    }

    override fun getChangePayload(oldItemPosition: Int, newItemPosition: Int): Any? {
        val newItem = newRatesList[newItemPosition]
        val diff = Bundle()
        diff.putString(AMOUNT_FIELD, newItem.amount.toString())
        return diff
    }

}