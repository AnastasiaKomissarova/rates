package akomissarova.currencyconverter.rates_list.ui

import akomissarova.currencyconverter.R
import akomissarova.currencyconverter.rates_list.data.PollingResult
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.rates_list_fragment.*
import org.koin.androidx.viewmodel.ext.android.getViewModel

class RateListFragment : Fragment() {

    companion object {
        val TAG = RateListFragment::class.java.name

        fun newInstance() =
            RateListFragment()
    }

    private lateinit var viewModel: RatesListViewModel
    private lateinit var adapter: RatesListAdapter

    private fun closeKeyboard() {
        activity?.let { activity ->
            val imm: InputMethodManager =
                activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            activity.currentFocus?.let {
                imm.hideSoftInputFromWindow(it.applicationWindowToken, InputMethodManager.SHOW_IMPLICIT)
            }
        }
    }

    private fun showKeyboard() {
        activity?.let {
            val imm: InputMethodManager =
                it.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.showSoftInput(it.currentFocus, InputMethodManager.SHOW_IMPLICIT)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.rates_list_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        adapter = RatesListAdapter(
            view.context,
            {
                viewModel.setBaseValue(it)
            }, {
                if (it) {
                    showKeyboard()
                } else {
                    closeKeyboard()
                }
            }) { chosenItem ->
            viewModel.setNewAmount(chosenItem)
        }
        initList()
        initViewModel()
    }

    private fun initViewModel() {
        viewModel = getViewModel()
        viewModel.also {
            it.getPollingResult().observe(viewLifecycleOwner, Observer { result ->
                showErrors(result)
            })
            it.startPolling()
            it.loadRates().observe(viewLifecycleOwner, Observer { currencyList ->
                adapter.updateList(currencyList)
            })
            it.triggerLocalData()
        }
    }

    private fun initList() {
        currencyList.layoutManager = LinearLayoutManager(context)
        currencyList.adapter = adapter
    }

    private fun showErrors(result: PollingResult) {
        when (result) {
            is PollingResult.Error -> showErrorToast()
        }
    }

    private fun showErrorToast() {
        activity?.let {
            Toast.makeText(it, getString(R.string.genericError), Toast.LENGTH_SHORT).show()
        }
    }
}
