package akomissarova.currencyconverter.rates_list.polling

import akomissarova.currencyconverter.rates_list.data.*
import akomissarova.currencyconverter.rates_list.network.IObservableNetwork
import akomissarova.currencyconverter.rates_list.repository.IRatesRepository
import akomissarova.currencyconverter.rates_list.rx.ISchedulerProvider
import akomissarova.currencyconverter.utils.ILocalization
import io.reactivex.Flowable
import java.util.concurrent.TimeUnit

class RatesGetter(
    private val repository: IRatesRepository,
    private val network: IObservableNetwork,
    private val schedulers: ISchedulerProvider,
    private val localization: ILocalization
) : IRatesGetter {

    override fun loadRates(): Flowable<List<RateItem>> {
        return repository.loadRates()
    }

    override fun startRatesPolling(
        currencyId: String
    ): Flowable<PollingResult> {
        return network.isConnectedState()
            .switchMap {
                if (it) {
                    getPeriodicUpdatesWithDataMerge(getCurrentBaseId(currencyId))
                } else {
                    updatePositionsOnError(currencyId, localization.getConnectionListError())
                }
            }
            .onErrorResumeNext { error: Throwable ->
                return@onErrorResumeNext updatePositionsOnError(currencyId, error.message ?: "")
            }
            .distinctUntilChanged()
            .subscribeOn(schedulers.io())
    }

    private fun getCurrentBaseId(currencyId: String) =
        if (currencyId.isEmpty()) repository.getRelevantBaseId() else currencyId

    private fun updatePositionsOnError(
        currencyId: String,
        error: String
    ): Flowable<PollingResult.Error> {
        return if (currencyId.isEmpty()) {
            getErrorFlowable(error)
        } else {
            repository.updateMainPositions(currencyId)
                .toFlowable()
                .flatMap {
                    getErrorFlowable(error)
                }
        }
    }

    private fun getErrorFlowable(error: String) =
        Flowable.just(PollingResult.Error(error))

    private fun getPeriodicUpdatesWithDataMerge(currencyId: String): Flowable<PollingResult> {
        return repository.getOnlineRatesUpdate(currencyId)
            .repeatWhen { result: Flowable<Any?> ->
                result.delay(1,
                    TimeUnit.SECONDS,
                    schedulers.computation())
            }
    }

    override fun recalculateValues(mainItem: RateItem): Flowable<List<RateItem>> {
        return repository.updateValuesByBaseItem(mainItem)
            .subscribeOn(schedulers.io())
            .onErrorReturn { emptyList() }
    }
}
