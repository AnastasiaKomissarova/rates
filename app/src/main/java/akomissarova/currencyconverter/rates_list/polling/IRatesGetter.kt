package akomissarova.currencyconverter.rates_list.polling

import akomissarova.currencyconverter.rates_list.data.PollingResult
import akomissarova.currencyconverter.rates_list.data.RateItem
import io.reactivex.Flowable

interface IRatesGetter {
    fun loadRates(): Flowable<List<RateItem>>
    fun startRatesPolling(currencyId: String): Flowable<PollingResult>
    fun recalculateValues(mainItem: RateItem): Flowable<List<RateItem>>
}
