package akomissarova.currencyconverter.rates_list.network

import android.content.Context
import android.content.Context.CONNECTIVITY_SERVICE
import android.net.ConnectivityManager
import android.net.Network
import android.net.NetworkCapabilities.NET_CAPABILITY_INTERNET
import android.net.NetworkInfo
import android.net.NetworkRequest
import android.os.Build
import io.reactivex.BackpressureStrategy
import io.reactivex.Flowable
import io.reactivex.subjects.BehaviorSubject
import timber.log.Timber

class ObservableNetwork(
    private val context: Context
) : IObservableNetwork {

    private val isConnectedSubject: BehaviorSubject<Boolean> = BehaviorSubject.create()
    private var networkCallback = object : ConnectivityManager.NetworkCallback() {
        override fun onAvailable(network: Network?) {
            isConnectedSubject.onNext(true)
        }

        override fun onLost(network: Network?) {
            isConnectedSubject.onNext(false)
        }
    }

    private val connectivityManager =
        context.getSystemService(CONNECTIVITY_SERVICE) as ConnectivityManager

    override fun isConnectedState(): Flowable<Boolean> {
        return isConnectedSubject
            .toFlowable(BackpressureStrategy.LATEST)
            .startWith(isConnectedSync()).distinctUntilChanged()
    }

    private fun isConnectedSync(): Boolean {
        val cm = context.getSystemService(CONNECTIVITY_SERVICE) as ConnectivityManager
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            cm.getNetworkCapabilities(cm.activeNetwork)?.hasCapability(NET_CAPABILITY_INTERNET) == true
        } else {
            val activeNetwork: NetworkInfo? = cm.activeNetworkInfo
            activeNetwork?.isConnectedOrConnecting ?: false
        }
    }

    override fun clear() {
        unregisterCallback()
    }

    override fun init() {
        registerCallback()
    }

    private fun unregisterCallback() {
        try {
            connectivityManager.unregisterNetworkCallback(networkCallback)
        } catch (exception: Exception) {
            Timber.w(exception)
        }
    }

    private fun registerCallback() {
        try {
            connectivityManager.registerNetworkCallback(
                NetworkRequest.Builder().build(),
                networkCallback
            )
        } catch (exception: Exception) {
            Timber.w(exception)
        }
    }

}
