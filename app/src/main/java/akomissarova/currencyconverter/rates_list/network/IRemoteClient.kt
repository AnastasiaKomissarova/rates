package akomissarova.currencyconverter.rates_list.network

import akomissarova.currencyconverter.rates_list.data.RatesResponse
import io.reactivex.Flowable

interface IRemoteClient {
    fun loadRates(currencyId: String): Flowable<RatesResponse>
}
