package akomissarova.currencyconverter.rates_list.network

import akomissarova.currencyconverter.rates_list.data.RatesResponse
import io.reactivex.Flowable
import retrofit2.http.GET
import retrofit2.http.Query

interface RatesService {

    @GET("latest")
    fun fetchRatesList(@Query("base") currencyId: String): Flowable<RatesResponse>

    @GET("latest")
    fun fetchRatesList(): Flowable<RatesResponse>
}
