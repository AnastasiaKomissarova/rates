package akomissarova.currencyconverter.rates_list.network

import io.reactivex.Flowable

interface IObservableNetwork {
    fun isConnectedState(): Flowable<Boolean>
    fun clear()
    fun init()
}
