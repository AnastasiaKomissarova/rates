package akomissarova.currencyconverter.rates_list.network

import akomissarova.currencyconverter.rates_list.data.RatesResponse
import io.reactivex.Flowable

class RemoteClient(val service: RatesService): IRemoteClient {

    override fun loadRates(currencyId: String): Flowable<RatesResponse> =
        if (currencyId.isEmpty()) {
            service.fetchRatesList()
        } else {
            service.fetchRatesList(currencyId)
        }

}
