package akomissarova.currencyconverter.rates_list.domain

import akomissarova.currencyconverter.rates_list.data.RateItem
import akomissarova.currencyconverter.rates_list.data.RatesResponse

interface IRatesMapper {
    fun mapResponseToList(response: RatesResponse): List<RateItem>
    fun mapListToNewBaseValue(list: List<RateItem>, baseItem: RateItem): List<RateItem>
    fun mapFreshToComplete(
        freshList: List<RateItem>,
        baseItem: RateItem,
        oldData: Map<String, RateItem>
    ): List<RateItem>
}
