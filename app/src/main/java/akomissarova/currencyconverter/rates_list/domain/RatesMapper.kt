package akomissarova.currencyconverter.rates_list.domain

import akomissarova.currencyconverter.rates_list.data.RateItem
import akomissarova.currencyconverter.rates_list.data.RatesResponse
import java.math.BigDecimal

class RatesMapper(private val ratesCalculator: IRatesCalculator) :
    IRatesMapper {
    override fun mapResponseToList(response: RatesResponse): List<RateItem> {
        val ratesList = mutableListOf(
            RateItem(
                response.baseCurrency,
                BigDecimal.ONE,
                BigDecimal.ZERO,
                0
            )
        )
        response.rates.entries.forEachIndexed { index, item ->
            ratesList.add(
                RateItem(
                    item.key,
                    item.value,
                    BigDecimal.ZERO,
                    index + 1
                )
            )
        }
        return ratesList.toList()
    }

    override fun mapListToNewBaseValue(list: List<RateItem>, baseItem: RateItem) =
        list.map {
            createDbRateItemFromDb(it, baseItem)
        }

    override fun mapFreshToComplete(
        freshList: List<RateItem>,
        baseItem: RateItem,
        oldData: Map<String, RateItem>
    ): List<RateItem> {
        return freshList.map { rateItem ->
            RateItem(
                id = rateItem.id,
                rate = rateItem.rate,
                amount = ratesCalculator.calculateAmount(baseItem, rateItem),
                position = ratesCalculator.getMovedPositionByBaseValue(
                    rateItem,
                    baseItem,
                    oldData[rateItem.id]?.position ?: oldData.size
                )
            )
        }
    }

    private fun createDbRateItemFromDb(
        it: RateItem,
        mainItem: RateItem
    ): RateItem {
        return RateItem(
            id = it.id,
            rate = it.rate,
            amount = ratesCalculator.getNewAmountByBaseValue(mainItem, it),
            position = ratesCalculator.getMovedPositionByBaseValue(it, mainItem, it.position)
        )
    }

}
