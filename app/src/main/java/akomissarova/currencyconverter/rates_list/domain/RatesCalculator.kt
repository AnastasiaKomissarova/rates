package akomissarova.currencyconverter.rates_list.domain

import akomissarova.currencyconverter.rates_list.data.RateItem
import java.math.BigDecimal
import java.math.MathContext

class RatesCalculator(private val mathContext: MathContext) :
    IRatesCalculator {
    override fun getNewAmountByBaseValue(baseItem: RateItem, rateItem: RateItem): BigDecimal =
        when (rateItem.id) {
            baseItem.id -> baseItem.amount
            else -> calculateRegularItemAmount(baseItem, rateItem).divide(
                baseItem.rate,
                mathContext
            )
                .stripTrailingZeros()
                .toPlainString()
                .toBigDecimal()
        }

    override fun getMovedPositionByBaseValue(
        rateItem: RateItem,
        baseItem: RateItem,
        oldPositionOrTail: Int
    ) = when {
        baseItem.id == rateItem.id -> 0
        oldPositionOrTail <= baseItem.position -> oldPositionOrTail + 1
        else -> oldPositionOrTail
    }

    override fun calculateAmount(baseItem: RateItem, rateItem: RateItem) = when (rateItem.id) {
        baseItem.id -> baseItem.amount
        else -> calculateRegularItemAmount(baseItem, rateItem)
    }

    private fun calculateRegularItemAmount(
        baseItem: RateItem,
        item: RateItem
    ): BigDecimal {
        return baseItem.amount.multiply(
            item.rate
        )
    }
}