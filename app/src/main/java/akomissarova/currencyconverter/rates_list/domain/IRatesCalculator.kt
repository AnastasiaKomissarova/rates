package akomissarova.currencyconverter.rates_list.domain

import akomissarova.currencyconverter.rates_list.data.RateItem
import java.math.BigDecimal

interface IRatesCalculator {
    fun getNewAmountByBaseValue(baseItem: RateItem, rateItem: RateItem): BigDecimal
    fun getMovedPositionByBaseValue(rateItem: RateItem, baseItem: RateItem, oldPositionOrTail: Int): Int
    fun calculateAmount(baseItem: RateItem, rateItem: RateItem): BigDecimal
}
