package akomissarova.currencyconverter.rates_list.repository

import akomissarova.currencyconverter.rates_list.data.AppDatabase
import akomissarova.currencyconverter.rates_list.data.PollingResult
import akomissarova.currencyconverter.rates_list.data.RateItem
import akomissarova.currencyconverter.rates_list.domain.IRatesMapper
import akomissarova.currencyconverter.rates_list.network.IRemoteClient
import akomissarova.currencyconverter.rates_list.rx.ISchedulerProvider
import io.reactivex.Flowable
import io.reactivex.Single

const val DEFAULT_CURRENCY = "EUR"

class RatesRepository(
    private val remoteClient: IRemoteClient,
    private val schedulers: ISchedulerProvider,
    private val ratesMapper: IRatesMapper,
    database: AppDatabase
) : IRatesRepository {

    private val rateDao = database.rateDao()

    override fun loadRates(): Flowable<List<RateItem>> {
        return rateDao.getAllFlowable()
            .onErrorReturn { emptyList() }
            .subscribeOn(schedulers.io())
    }

    override fun getRelevantBaseId(): String {
        val firstItem = rateDao.getFirst()
        return firstItem?.id ?: DEFAULT_CURRENCY
    }

    private fun getAllDbAsMap(): Map<String, RateItem>? =
        rateDao.getAll().map {
            it.id to it
        }.toMap()

    private fun loadRemoteRates(currencyId: String): Flowable<List<RateItem>> =
        remoteClient.loadRates(currencyId).map { response ->
            return@map ratesMapper.mapResponseToList(response)
        }

    override fun getOnlineRatesUpdate(currencyId: String): Flowable<PollingResult> {
        return loadRemoteRates(currencyId)
            .subscribeOn(schedulers.io())
            .flatMap { onlineList ->
                processAndRecordList(currencyId, onlineList)
            }.flatMap {
                Flowable.just(PollingResult.Success)
            }
    }

    private fun processAndRecordList(currencyId: String, onlineList: List<RateItem>): Flowable<Boolean> =
        Flowable.fromCallable {
            val dbMap: Map<String, RateItem>? = getAllDbAsMap()
            val dbMainItem = dbMap?.get(currencyId)
            rateDao.updateAllInTransaction(
                if (dbMap == null || dbMainItem == null) onlineList else ratesMapper.mapFreshToComplete(
                    onlineList,
                    dbMainItem,
                    dbMap
                )
            )
        }
            .map { true }

    override fun updateMainPositions(currencyId: String): Single<Boolean> {
        return rateDao.getAllSingle()
            .map { list ->
                val mainItem = rateDao.get(currencyId)
                if (mainItem == null) {
                    list
                } else {
                    ratesMapper.mapListToNewBaseValue(list, mainItem)
                }
                    .also {
                        rateDao.updateAllInTransaction(it)
                    }
            }
            .flatMap {
                Single.just(true)
            }
            .subscribeOn(schedulers.io())
    }

    override fun updateValuesByBaseItem(mainItem: RateItem) =
        rateDao.getAllSingle()
            .map { list ->
                ratesMapper.mapListToNewBaseValue(list, mainItem)
                    .also {
                        rateDao.updateAllInTransaction(it)
                    }
            }
            .toFlowable().flatMap {
                loadRates()
            }
            .subscribeOn(schedulers.io())
}
