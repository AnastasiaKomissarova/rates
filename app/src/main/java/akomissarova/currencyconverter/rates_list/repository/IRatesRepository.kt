package akomissarova.currencyconverter.rates_list.repository

import akomissarova.currencyconverter.rates_list.data.PollingResult
import akomissarova.currencyconverter.rates_list.data.RateItem
import akomissarova.currencyconverter.rates_list.data.RatesResponse
import io.reactivex.Flowable
import io.reactivex.Single

interface IRatesRepository {
    fun loadRates(): Flowable<List<RateItem>>
    fun getRelevantBaseId(): String
    fun getOnlineRatesUpdate(currencyId: String): Flowable<PollingResult>
    fun updateMainPositions(currencyId: String): Single<Boolean>
    fun updateValuesByBaseItem(mainItem: RateItem): Flowable<List<RateItem>>
}
