package akomissarova.currencyconverter.rates_list.rx

import io.reactivex.Scheduler

interface ISchedulerProvider {
    fun io(): Scheduler
    fun computation(): Scheduler
}
