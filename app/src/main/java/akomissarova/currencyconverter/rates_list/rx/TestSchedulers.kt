package akomissarova.currencyconverter.rates_list.rx

import io.reactivex.Scheduler
import io.reactivex.schedulers.TestScheduler

class TestSchedulers : ISchedulerProvider {

    private val scheduler = TestScheduler()

    override fun io(): Scheduler = scheduler

    override fun computation() = scheduler

}
