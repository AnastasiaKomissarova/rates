package akomissarova.currencyconverter.rates_list.rx

import io.reactivex.schedulers.Schedulers

class ImmediateSchedulers: ISchedulerProvider {
    override fun io() =
        Schedulers.trampoline()

    override fun computation() = Schedulers.trampoline()

}
