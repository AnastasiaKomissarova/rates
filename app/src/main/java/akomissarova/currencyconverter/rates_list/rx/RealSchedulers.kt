package akomissarova.currencyconverter.rates_list.rx

import io.reactivex.schedulers.Schedulers

class RealSchedulers: ISchedulerProvider {
    override fun io() = Schedulers.io()

    override fun computation() = Schedulers.computation()
}
