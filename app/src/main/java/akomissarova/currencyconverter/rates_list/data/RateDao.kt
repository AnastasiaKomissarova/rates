package akomissarova.currencyconverter.rates_list.data

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Transaction
import io.reactivex.Flowable
import io.reactivex.Single
import java.nio.file.Files.delete


@Dao
interface RateDao {

    @Query("SELECT * FROM $ratesTable ORDER BY position ASC")
    fun getAllFlowable(): Flowable<List<RateItem>>

    @Query("SELECT * FROM $ratesTable ORDER BY position ASC")
    fun getAllSingle(): Single<List<RateItem>>

    @Query("SELECT * FROM $ratesTable ORDER BY position ASC")
    fun getAll(): List<RateItem>

    @Query("DELETE FROM $ratesTable")
    fun removeAll()

    @Insert
    fun insertAll(rates: List<RateItem>)

    @Transaction
    fun updateAllInTransaction(rates: List<RateItem>) {
        removeAll()
        insertAll(rates)
    }

    @Query("SELECT * FROM $ratesTable where id = (:id)")
    fun get(id: String): RateItem?

    @Query("SELECT * FROM $ratesTable ORDER BY position ASC LIMIT 1")
    fun getFirst(): RateItem?

}
