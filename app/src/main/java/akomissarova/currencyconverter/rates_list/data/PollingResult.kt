package akomissarova.currencyconverter.rates_list.data

sealed class PollingResult {
    object Success : PollingResult()
    class Error(val message: String) : PollingResult()
}
