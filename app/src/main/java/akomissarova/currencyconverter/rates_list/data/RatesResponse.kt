package akomissarova.currencyconverter.rates_list.data

import java.math.BigDecimal

data class RatesResponse (
    val baseCurrency: String,
    val rates: Map<String, BigDecimal>
)