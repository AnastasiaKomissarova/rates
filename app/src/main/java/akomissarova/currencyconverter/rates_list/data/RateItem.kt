package akomissarova.currencyconverter.rates_list.data

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.math.BigDecimal

@Entity(tableName = ratesTable)
data class RateItem(
    @PrimaryKey val id: String,
    @ColumnInfo(name = "rate") val rate: BigDecimal,
    @ColumnInfo(name = "amount") val amount: BigDecimal = BigDecimal.ZERO,
    @ColumnInfo(name = "position") val position: Int = 0
)

const val ratesTable = "rates"

