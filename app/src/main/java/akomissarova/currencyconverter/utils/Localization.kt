package akomissarova.currencyconverter.utils

import akomissarova.currencyconverter.R
import android.content.Context

class Localization(private val context: Context): ILocalization {

    override fun getConnectionListError(): String {
        return context.getString(R.string.connection_lost)
    }

}
