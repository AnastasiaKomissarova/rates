package akomissarova.currencyconverter

import akomissarova.currencyconverter.rates_list.ui.RateListFragment
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if (savedInstanceState == null) {
            addListFragment()
        }
    }

    private fun addListFragment() {
        supportFragmentManager.beginTransaction().add(
            R.id.fragment_container,
            RateListFragment.newInstance(),
            RateListFragment.TAG
        ).commit()
    }
}
