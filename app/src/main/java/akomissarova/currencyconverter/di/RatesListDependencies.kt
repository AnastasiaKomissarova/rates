package akomissarova.currencyconverter.di

import akomissarova.currencyconverter.BuildConfig
import akomissarova.currencyconverter.R
import akomissarova.currencyconverter.rates_list.data.*
import akomissarova.currencyconverter.rates_list.domain.*
import akomissarova.currencyconverter.rates_list.network.*
import akomissarova.currencyconverter.rates_list.polling.IRatesGetter
import akomissarova.currencyconverter.rates_list.polling.RatesGetter
import akomissarova.currencyconverter.rates_list.repository.IRatesRepository
import akomissarova.currencyconverter.rates_list.repository.RatesRepository
import akomissarova.currencyconverter.rates_list.rx.ISchedulerProvider
import akomissarova.currencyconverter.rates_list.rx.RealSchedulers
import akomissarova.currencyconverter.rates_list.ui.RatesListViewModel
import akomissarova.currencyconverter.utils.ILocalization
import akomissarova.currencyconverter.utils.Localization
import androidx.room.Room
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.math.MathContext
import java.math.RoundingMode

val ratesDomain = module {
    single<IRatesGetter> {
        RatesGetter(
            get(),
            get(),
            get(),
            get()
        )
    }
    single { MathContext(9, RoundingMode.HALF_EVEN) }
    single<IRatesCalculator> {
        RatesCalculator(
            get()
        )
    }
}

val ratesListViewModelModule = module {
    viewModel { RatesListViewModel(get(), get()) }
}

val ratesListDataModule = module {
    single<IRatesRepository> {
        RatesRepository(
            get(),
            get(),
            get(),
            get()
        )
    }
    single<IRatesMapper> {
        RatesMapper(
            get()
        )
    }
}

val networkModule = module {
    single {
        val loggingInterceptor = HttpLoggingInterceptor(object : HttpLoggingInterceptor.Logger {
            override fun log(message: String) {
                //Timber.tag("OkHttp").d(message)
            }
        })
        loggingInterceptor.setLevel(if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BODY else HttpLoggingInterceptor.Level.BASIC)
        OkHttpClient.Builder()
            .addInterceptor(loggingInterceptor)
            .build()
    }

    single {
        Retrofit.Builder()
            .client(get<OkHttpClient>())
            .baseUrl(androidContext().getString(R.string.baseUrl))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    single { get<Retrofit>().create(RatesService::class.java) }

    single<IRemoteClient> { RemoteClient(get())  }

    single<IObservableNetwork> { ObservableNetwork(get()) }
}

val databaseModule = module {
    single {
        Room.databaseBuilder(
            get(),
            AppDatabase::class.java, "currency_rates"
        ).build()
    }
}

val utilsModule = module {
    single<ISchedulerProvider> { RealSchedulers() }
    single<ILocalization> { Localization(get()) }
}
