package akomissarova.currencyconverter

import akomissarova.currencyconverter.di.*
import android.app.Application
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import timber.log.Timber
import timber.log.Timber.DebugTree


class CurrencyConverterApp : Application() {
    override fun onCreate() {
        super.onCreate()

        if (BuildConfig.DEBUG) {
            Timber.plant(DebugTree())
        }

        startKoin {
            androidContext(applicationContext)
            modules(listOf(networkModule, ratesListViewModelModule, ratesListDataModule, databaseModule, utilsModule, ratesDomain))
        }
    }
}