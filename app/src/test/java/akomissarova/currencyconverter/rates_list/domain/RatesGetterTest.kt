package akomissarova.currencyconverter.rates_list.domain

import akomissarova.currencyconverter.rates_list.data.PollingResult
import akomissarova.currencyconverter.rates_list.data.RatesResponse
import akomissarova.currencyconverter.rates_list.network.IObservableNetwork
import akomissarova.currencyconverter.rates_list.polling.RatesGetter
import akomissarova.currencyconverter.rates_list.repository.IRatesRepository
import akomissarova.currencyconverter.rates_list.repository.RatesTestContent
import akomissarova.currencyconverter.rates_list.rx.ISchedulerProvider
import akomissarova.currencyconverter.rates_list.rx.TestSchedulers
import akomissarova.currencyconverter.utils.Localization
import com.nhaarman.mockitokotlin2.verify
import io.reactivex.Flowable
import io.reactivex.Single
import io.reactivex.schedulers.TestScheduler
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.MockitoAnnotations
import java.util.concurrent.TimeUnit

@RunWith(JUnit4::class)
class RatesGetterTest {

    @Mock
    lateinit var network: IObservableNetwork

    @Mock
    lateinit var repository: IRatesRepository

    @Mock
    lateinit var localization: Localization

    private lateinit var schedulers: ISchedulerProvider
    private lateinit var ratesGetter: RatesGetter

    @Before
    fun setUp() {
        MockitoAnnotations.openMocks(this)
        schedulers = TestSchedulers()

        ratesGetter =
            RatesGetter(
                repository = repository,
                network = network,
                schedulers = schedulers,
                localization = localization
            )
        `when`(localization.getConnectionListError()).thenReturn("error")
    }

    @Test
    fun `load rates`() {
        val list = RatesTestContent.createDbListDefault()
        `when`(repository.loadRates()).thenReturn(Flowable.just(list))

        val testObserver = ratesGetter.loadRates().test()

        testObserver
            .assertNoErrors()
            .assertResult(list)

        testObserver.dispose()
    }

    @Test
    fun `start polling when connected`() {
        val lastBaseCurrency = "USD"
        val response = RatesResponse(
            lastBaseCurrency,
            RatesTestContent.createOnlineMapForChangedCurrency()
        )

        `when`(network.isConnectedState()).thenReturn(Flowable.just(true))
        `when`(repository.getOnlineRatesUpdate(lastBaseCurrency)).thenReturn(Flowable.just(PollingResult.Success))

        val testObserver = ratesGetter.startRatesPolling(lastBaseCurrency).test()
        val testScheduler = schedulers.io() as TestScheduler

        testScheduler.advanceTimeBy(500, TimeUnit.MILLISECONDS)

        verify(repository).getOnlineRatesUpdate(lastBaseCurrency)
        testObserver.assertValue(PollingResult.Success)

        testObserver.dispose()
    }

    @Test
    fun `start polling when no connection return error`() {

        val lastBaseCurrency = "USD"
        `when`(network.isConnectedState()).thenReturn(Flowable.just(false))
        `when`(repository.updateMainPositions(lastBaseCurrency)).thenReturn(Single.just(true))

        val testObserver = ratesGetter.startRatesPolling(lastBaseCurrency).test()
        val testScheduler = schedulers.io() as TestScheduler

        testScheduler.advanceTimeBy(500, TimeUnit.MILLISECONDS)
        verify(repository).updateMainPositions(lastBaseCurrency)
        testObserver.assertValue {
            it is PollingResult.Error
        }

        testObserver.dispose()
    }
}