package akomissarova.currencyconverter.rates_list.domain

import akomissarova.currencyconverter.rates_list.repository.RatesTestContent
import io.reactivex.Flowable
import org.junit.Before
import org.junit.Test

import org.junit.Assert.*
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.Mockito.`when`
import org.mockito.MockitoAnnotations
import java.math.BigDecimal
import java.math.MathContext
import java.math.RoundingMode

@RunWith(JUnit4::class)
class RatesCalculatorTest {

    @Mock
    lateinit var mathContext: MathContext

    private lateinit var calculator: RatesCalculator

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        calculator = RatesCalculator(mathContext)
        `when`(mathContext.precision).thenReturn(9)
        `when`(mathContext.roundingMode).thenReturn(RoundingMode.HALF_EVEN)
    }

    @Test
    fun `get new amount by base value when different items`() {
        assertEquals(
            BigDecimal.valueOf(6),
            calculator.getNewAmountByBaseValue(
                RatesTestContent.createBaseItemDefault(),
                RatesTestContent.createNonBaseItem()
            )
        )
    }

    @Test
    fun `get new amount by base value when same item`() {
        val item = RatesTestContent.createBaseItemDefault()
        val amount = item.amount
        assertEquals(
            amount,
            calculator.getNewAmountByBaseValue(
                item,
                item
            )
        )
    }

    @Test
    fun `get moved position by base value when same item`() {
        val item = RatesTestContent.createBaseItemDefault()
        val position = item.position
        assertEquals(
            position,
            calculator.getMovedPositionByBaseValue(
                item,
                item,
                0
            )
        )
    }

    @Test
    fun `get moved position by base value when different items and old position is less than base item position`() {
        assertEquals(
            1,
            calculator.getMovedPositionByBaseValue(
                RatesTestContent.createBaseItemDefault(),
                RatesTestContent.createNonBaseItem(),
                0
            )
        )
    }

    @Test
    fun `get moved position by base value when different items and old position is more than base item position`() {
        assertEquals(
            6,
            calculator.getMovedPositionByBaseValue(
                RatesTestContent.createBaseItemDefault(),
                RatesTestContent.createNonBaseItem(),
                6
            )
        )
    }

    @Test
    fun `calculate amount when items are the same`() {
        val item = RatesTestContent.createBaseItemDefault()
        val amount = item.amount
        assertEquals(
            amount,
            calculator.calculateAmount(
                item,
                item
            )
        )
    }

    @Test
    fun `calculate amount when items are different`() {
        assertEquals(
            BigDecimal.valueOf(6.0),
            calculator.calculateAmount(
                RatesTestContent.createBaseItemDefault(),
                RatesTestContent.createNonBaseItem()
            )
        )
    }
}