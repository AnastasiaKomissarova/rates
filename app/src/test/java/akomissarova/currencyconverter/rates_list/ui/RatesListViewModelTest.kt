package akomissarova.currencyconverter.rates_list.ui

import akomissarova.currencyconverter.rates_list.data.PollingResult
import akomissarova.currencyconverter.rates_list.data.RateItem
import akomissarova.currencyconverter.rates_list.polling.IRatesGetter
import akomissarova.currencyconverter.rates_list.network.IObservableNetwork
import akomissarova.currencyconverter.rates_list.repository.RatesTestContent
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import io.reactivex.Flowable
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.Mockito.verify
import org.mockito.MockitoAnnotations

@RunWith(JUnit4::class)
class RatesListViewModelTest {

    @get:Rule
    val taskExecutorRule = InstantTaskExecutorRule()

    @Mock
    lateinit var ratesGetter: IRatesGetter

    @Mock
    lateinit var network: IObservableNetwork

    private lateinit var viewModel: RatesListViewModel

    @Before
    fun setUp() {
        MockitoAnnotations.openMocks(this)
        viewModel = RatesListViewModel(network, ratesGetter)
    }

    @Test
    fun setBaseValue() {
        val result = mutableListOf<PollingResult>()
        val baseValue = RatesTestContent.CHANGED_ID
        val sequence = listOf(PollingResult.Success, PollingResult.Error("Error"))
        `when`(ratesGetter.startRatesPolling(baseValue)).thenReturn(Flowable.fromArray(*sequence.toTypedArray()))
        viewModel.getPollingResult().observeForever {
            result.add(it)
        }

        viewModel.setBaseValue(baseValue)

        verify(ratesGetter).startRatesPolling(baseValue)
        assertEquals(sequence, result)

        viewModel.setBaseValue(baseValue)

        verify(ratesGetter).startRatesPolling(baseValue)
    }

    @Test
    fun setNewAmount() {
        var result = emptyList<RateItem>()
        val list = RatesTestContent.createDbListDefault()
        val newItem = RatesTestContent.createBaseItemDefaultEdited()
        `when`(ratesGetter.recalculateValues(newItem)).thenReturn(
            Flowable.just(list)
        )
        viewModel.loadRates().observeForever {
            result = it
        }

        viewModel.setNewAmount(newItem)

        verify(ratesGetter).recalculateValues(newItem)
        assertEquals(list, result)

        viewModel.setNewAmount(newItem)

        verify(ratesGetter).recalculateValues(newItem)
    }

    @Test
    fun triggerLocalData() {
        var result = emptyList<RateItem>()
        val list = RatesTestContent.createDbListDefault()
        `when`(ratesGetter.loadRates()).thenReturn(
            Flowable.just(list)
        )
        viewModel.loadRates().observeForever {
            result = it
        }

        viewModel.triggerLocalData()

        verify(ratesGetter).loadRates()
        assertEquals(list, result)
    }

    @Test
    fun startPolling() {
        val result = mutableListOf<PollingResult>()
        val sequence = listOf(PollingResult.Success, PollingResult.Error("Error"))
        `when`(ratesGetter.startRatesPolling("")).thenReturn(Flowable.fromArray(*sequence.toTypedArray()))
        viewModel.getPollingResult().observeForever {
            result.add(it)
        }

        viewModel.startPolling()

        verify(ratesGetter).startRatesPolling("")
        verify(network).init()
        assertEquals(sequence, result)
    }

}