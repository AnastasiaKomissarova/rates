package akomissarova.currencyconverter.rates_list.repository

import akomissarova.currencyconverter.rates_list.data.RateItem
import java.math.BigDecimal

class RatesTestContent {
    companion object {
        internal fun createDbListDefault(): List<RateItem> {
            return listOf(
                RateItem(
                    id = "EUR",
                    rate = BigDecimal.ONE,
                    amount = BigDecimal.valueOf(5),
                    position = 0
                ),
                RateItem(
                    id = "USD",
                    rate = BigDecimal.valueOf(1.2),
                    amount = BigDecimal.valueOf(6),
                    position = 1
                ),
                RateItem(
                    id = "PLN",
                    rate = BigDecimal.valueOf(4.54),
                    amount = BigDecimal.valueOf(22.7),
                    position = 2
                )
            )
        }

        internal fun createDbMapDefault(): Map<String, RateItem> {
            return mapOf(
                "EUR" to RateItem(
                    id = "EUR",
                    rate = BigDecimal.ONE,
                    amount = BigDecimal.valueOf(5),
                    position = 0
                ),
                "USD" to RateItem(
                    id = "USD",
                    rate = BigDecimal.valueOf(1.2),
                    amount = BigDecimal.valueOf(6),
                    position = 1
                ),
                "PLN" to RateItem(
                    id = "PLN",
                    rate = BigDecimal.valueOf(4.54),
                    amount = BigDecimal.valueOf(22.7),
                    position = 2
                )
            )
        }

        internal fun createSampleDbListChanged(): List<RateItem> {
            return listOf(
                RateItem(
                    id = "USD",
                    rate = BigDecimal.ONE,
                    amount = BigDecimal.valueOf(5),
                    position = 0
                ),
                RateItem(
                    id = "EUR",
                    rate = BigDecimal.valueOf(1.2),
                    amount = BigDecimal.valueOf(6),
                    position = 1
                ),
                RateItem(
                    id = "PLN",
                    rate = BigDecimal.valueOf(4.54),
                    amount = BigDecimal.valueOf(22.7),
                    position = 2
                )
            )
        }

        fun createOnlineMapForDefaultCurrency(): Map<String, BigDecimal> {
            return mapOf("USD" to BigDecimal.valueOf(1.24), "PLN" to BigDecimal.valueOf(4.56))
        }

        fun createOnlineMapForChangedCurrency(): Map<String, BigDecimal> {
            return mapOf("EUR" to BigDecimal.valueOf(1.24), "PLN" to BigDecimal.valueOf(4.56))
        }

        fun createDefaultListFromOnline(): List<RateItem> {
            return listOf(
                RateItem(
                    id = "EUR",
                    rate = BigDecimal.ONE,
                    amount = BigDecimal.ZERO,
                    position = 0
                ),
                RateItem(
                    id = "USD",
                    rate = BigDecimal.valueOf(1.23),
                    amount = BigDecimal.ZERO,
                    position = 1
                ),
                RateItem(
                    id = "PLN",
                    rate = BigDecimal.valueOf(4.53),
                    amount = BigDecimal.ZERO,
                    position = 2
                )
            )

        }

        fun createChangedListFromOnline(): List<RateItem> {
            return listOf(
                RateItem(
                    id = "USD",
                    rate = BigDecimal.ONE,
                    amount = BigDecimal.ZERO,
                    position = 0
                ),
                RateItem(
                    id = "EUR",
                    rate = BigDecimal.valueOf(1.24),
                    amount = BigDecimal.ZERO,
                    position = 1
                ),
                RateItem(
                    id = "PLN",
                    rate = BigDecimal.valueOf(4.56),
                    amount = BigDecimal.ZERO,
                    position = 2
                )
            )

        }

        fun createUpdatedListWhenBaseChanged(): List<RateItem> {
            return listOf(
                RateItem(
                    id = "USD",
                    rate = BigDecimal.ONE,
                    amount = BigDecimal.valueOf(6),
                    position = 0
                ),
                RateItem(
                    id = "EUR",
                    rate = BigDecimal.valueOf(1.24),
                    amount = BigDecimal.valueOf(7.44),
                    position = 1
                ),
                RateItem(
                    id = "PLN",
                    rate = BigDecimal.valueOf(4.56),
                    amount = BigDecimal.valueOf(27.36),
                    position = 2
                )
            )

        }

        fun createBaseItemDefault() = RateItem(
            id = "EUR",
            rate = BigDecimal.ONE,
            amount = BigDecimal.valueOf(5),
            position = 0
        )

        fun createNonBaseItem() = RateItem(
            id = "USD",
            rate = BigDecimal.valueOf(1.2),
            amount = BigDecimal.valueOf(7.5),
            position = 4
        )

        fun createBaseItemDefaultEdited() = RateItem(
            id = "EUR",
            rate = BigDecimal.ONE,
            amount = BigDecimal.valueOf(50),
            position = 0
        )

        fun createMainItemChangedWithoutRateUpdate() = RateItem(
            id = "USD",
            rate = BigDecimal.valueOf(1.2),
            amount = BigDecimal.valueOf(6),
            position = 0
        )

        fun createUpdatedListWhenTheSame(): List<RateItem> =
            listOf(
                RateItem(
                    id = "EUR",
                    rate = BigDecimal.ONE,
                    amount = BigDecimal.valueOf(5),
                    position = 0
                ),
                RateItem(
                    id = "USD",
                    rate = BigDecimal.valueOf(1.23),
                    amount = BigDecimal.valueOf(6.15),
                    position = 1
                ),
                RateItem(
                    id = "PLN",
                    rate = BigDecimal.valueOf(4.53),
                    amount = BigDecimal.valueOf(22.65),
                    position = 2
                )
            )

        fun createRecalculatedDbListDefault(): List<RateItem> =
            listOf(
                RateItem(
                    id = "EUR",
                    rate = BigDecimal.ONE,
                    amount = BigDecimal.valueOf(50),
                    position = 0
                ),
                RateItem(
                    id = "USD",
                    rate = BigDecimal.valueOf(1.2),
                    amount = BigDecimal.valueOf(60),
                    position = 1
                ),
                RateItem(
                    id = "PLN",
                    rate = BigDecimal.valueOf(4.54),
                    amount = BigDecimal.valueOf(227),
                    position = 2
                )
            )

        fun createRecalculatedDbListChanged(): List<RateItem> =
            listOf(
                RateItem(
                    id = "EUR",
                    rate = BigDecimal.ONE,
                    amount = BigDecimal.valueOf(5),
                    position = 1
                ),
                RateItem(
                    id = "USD",
                    rate = BigDecimal.valueOf(1.2),
                    amount = BigDecimal.valueOf(6),
                    position = 0
                ),
                RateItem(
                    id = "PLN",
                    rate = BigDecimal.valueOf(4.54),
                    amount = BigDecimal.valueOf(22.7),
                    position = 2
                )
            )

        const val CHANGED_ID = "USD"

    }
}




