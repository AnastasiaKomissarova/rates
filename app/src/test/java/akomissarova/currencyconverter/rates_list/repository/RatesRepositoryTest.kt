package akomissarova.currencyconverter.rates_list.repository

import akomissarova.currencyconverter.rates_list.data.AppDatabase
import akomissarova.currencyconverter.rates_list.data.PollingResult
import akomissarova.currencyconverter.rates_list.data.RateDao
import akomissarova.currencyconverter.rates_list.data.RatesResponse
import akomissarova.currencyconverter.rates_list.domain.IRatesMapper
import akomissarova.currencyconverter.rates_list.network.IRemoteClient
import akomissarova.currencyconverter.rates_list.rx.ISchedulerProvider
import akomissarova.currencyconverter.rates_list.rx.ImmediateSchedulers
import io.reactivex.Flowable
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.verify
import io.reactivex.Single
import org.mockito.Mockito.`when`

@RunWith(JUnit4::class)
class RatesRepositoryTest {

    @Mock
    lateinit var remoteClient: IRemoteClient

    @Mock
    lateinit var database: AppDatabase

    @Mock
    lateinit var rateDao: RateDao

    @Mock
    lateinit var ratesMapper: IRatesMapper

    private lateinit var schedulers: ISchedulerProvider
    private lateinit var repository: RatesRepository

    @Before
    fun setUp() {
        MockitoAnnotations.openMocks(this)
        `when`(database.rateDao()).thenReturn(rateDao)
        schedulers = ImmediateSchedulers()
        repository =
            RatesRepository(
                remoteClient,
                schedulers,
                ratesMapper,
                database
            )
    }

    @Test
    fun `load rates from db without errors`() {
        val dbList = RatesTestContent.createDbListDefault()
        `when`(rateDao.getAllFlowable()).thenReturn(Flowable.just(dbList))

        val testObserver = repository.loadRates().test()

        testObserver
            .assertNoErrors()
            .assertResult(dbList)

        testObserver.dispose()
    }

    @Test
    fun `load rates from db with error`() {
        val error = RuntimeException("error")
        `when`(rateDao.getAllFlowable()).thenReturn(Flowable.error(error))

        val testObserver = repository.loadRates().test()

        testObserver
            .assertResult(emptyList())

        testObserver.dispose()
    }

    @Test
    fun `get relevant base id when present in database`() {
        val item = RatesTestContent.createBaseItemDefault()
        `when`(rateDao.getFirst()).thenReturn(item)

        assertEquals(item.id, repository.getRelevantBaseId())
    }

    @Test
    fun `get relevant base id when not present in database`() {
        `when`(rateDao.getFirst()).thenReturn(null)

        assertEquals(DEFAULT_CURRENCY, repository.getRelevantBaseId())
    }

    @Test
    fun `get online rate updates when empty db`() {
        val currency = DEFAULT_CURRENCY
        val response = RatesResponse(
            currency,
            RatesTestContent.createOnlineMapForDefaultCurrency()
        )

        `when`(ratesMapper.mapResponseToList(response)).thenReturn(RatesTestContent.createDefaultListFromOnline())
        `when`(remoteClient.loadRates(currency)).thenReturn(Flowable.just(response))

        val testObserver = repository.getOnlineRatesUpdate(currency).test()

        verify(ratesMapper).mapResponseToList(response)

        testObserver.assertResult(PollingResult.Success)

        testObserver.dispose()
    }

    @Test
    fun `get online rate updates when not empty db`() {
        val currency = DEFAULT_CURRENCY
        val response = RatesResponse(
            currency,
            RatesTestContent.createOnlineMapForDefaultCurrency()
        )
        val dbList = RatesTestContent.createDbListDefault()
        val freshList = RatesTestContent.createDefaultListFromOnline()

        `when`(ratesMapper.mapResponseToList(response)).thenReturn(RatesTestContent.createDefaultListFromOnline())
        `when`(ratesMapper.mapFreshToComplete(any(), any(), any())).thenReturn(RatesTestContent.createDefaultListFromOnline())
        `when`(rateDao.getAll()).thenReturn(dbList)
        `when`(remoteClient.loadRates(currency)).thenReturn(Flowable.just(response))

        val testObserver = repository.getOnlineRatesUpdate(currency).test()

        verify(ratesMapper).mapResponseToList(response)
        verify(ratesMapper).mapFreshToComplete(freshList, RatesTestContent.createBaseItemDefault(), RatesTestContent.createDbMapDefault())
        testObserver.assertResult(PollingResult.Success)

        testObserver.dispose()
    }

    @Test
    fun `update main positions when main item null`() {
        val currency = DEFAULT_CURRENCY

        val dbList = RatesTestContent.createDbListDefault()
        RatesTestContent.createDefaultListFromOnline()

        `when`(rateDao.getAllSingle()).thenReturn(Single.just(dbList))

        val testObserver = repository.updateMainPositions(currency).test()

        verify(rateDao).get(currency)
        verify(rateDao).updateAllInTransaction(dbList)
        testObserver.assertResult(true)

        testObserver.dispose()
    }

    @Test
    fun `update main positions when main item not null`() {
        val currency = DEFAULT_CURRENCY
        val dbList = RatesTestContent.createDbListDefault()
        val mainItem = RatesTestContent.createBaseItemDefault()
        val updatedList = RatesTestContent.createUpdatedListWhenBaseChanged()

        `when`(rateDao.getAllSingle()).thenReturn(Single.just(dbList))
        `when`(rateDao.get(any())).thenReturn(mainItem)
        `when`(ratesMapper.mapListToNewBaseValue(dbList, mainItem)).thenReturn(updatedList)

        val testObserver = repository.updateMainPositions(currency).test()

        verify(rateDao).get(currency)
        verify(ratesMapper).mapListToNewBaseValue(dbList, mainItem)
        verify(rateDao).updateAllInTransaction(updatedList)
        testObserver.assertResult(true)

        testObserver.dispose()
    }

    @Test
    fun `update value by base item`() {
        val dbList = RatesTestContent.createDbListDefault()
        val mainItem = RatesTestContent.createBaseItemDefault()
        val newBaseItem = RatesTestContent.createMainItemChangedWithoutRateUpdate()
        val updatedList = RatesTestContent.createUpdatedListWhenBaseChanged()

        `when`(rateDao.getAllSingle()).thenReturn(Single.just(dbList))
        `when`(ratesMapper.mapListToNewBaseValue(dbList, mainItem)).thenReturn(updatedList)
        `when`(rateDao.getAllFlowable()).thenReturn(Flowable.just(updatedList))

        val testObserver = repository.updateValuesByBaseItem(newBaseItem).test()

        testObserver.assertResult(updatedList)

        testObserver.dispose()
    }
}